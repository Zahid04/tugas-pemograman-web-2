<?php
 // memanggil file koneksi.php untuk membuat koneksi
include 'koneksi.php';
 // mengecek apakah di url ada nilai GET id
 if (isset($_GET['id_matkul'])) {
 // ambil nilai id dari url dan disimpan dalam variabel $id
 $id_matkul = ($_GET["id_matkul"]);
 // menampilkan data dari database yang mempunyai id=$id
 $query = "SELECT * FROM matkul WHERE id_matkul='$id_matkul'";
 $result = mysqli_query($koneksi, $query);
 // jika data gagal diambil maka akan tampil error berikut
 if(!$result){
 die ("Query Error: ".mysqli_errno($koneksi).
 " - ".mysqli_error($koneksi));
 }
 // mengambil data dari database
 $data = mysqli_fetch_assoc($result);
 // apabila data tidak ada pada database maka akan dijalankan perintah ini
 if (!count($data)) {
 echo "<script>alert('Data tidak ditemukan pada
database');window.location='index.php';</script>";
 }
 } else {
 // apabila tidak ada data GET id pada akan di redirect ke index.php
 echo "<script>alert('Masukkan data id_matkul.');window.location='index.php';</script>";
 } 
 ?>
<!DOCTYPE html>
<html>
 <head>
 <title>CRUD Data mahasiswa dengan gambar - Mahasiswa UMMY Solok 2022</title>
 <style type="text/css">
 * {
      font-family: "Trebuchet MS";
      }
      h1 {
      text-transform: uppercase;
      color: salmon;
      }
      button {
      background-color: salmon;
      color: #fff;
      padding: 10px;
      text-decoration: none;
      font-size: 12px;
      border: 0px;
      margin-top: 20px;
      }
      label {
      margin-top: 10px;
      float: left;
      text-align: left;
      width: 100%;
      }
      input {
      padding: 6px;
      width: 100%;
      box-sizing: border-box;
      background: #f8f8f8;
      border: 2px solid #ccc;
      outline-color: salmon;
      }
      div {
         width: 100%;
      height: auto;
      }
      .base {
      width: 400px;
      height: auto;
      padding: 20px;
      margin-left: auto;
      margin-right: auto;
      background: #ededed;
 }
 </style>
 </head>
 <body>
 <center>
 <h1>Edit Data Matakuliah <?php echo $data['nama_matkul']; ?></h1>
 <center>
 <form method="POST" action="edit_matkul.php" enctype="multipart/form-data" >
 <section class="base">
 <!-- menampung nilai id produk yang akan di edit -->
 <input name="id_matkul" value="<?php echo $data['id_matkul']; ?>" hidden />
 <div>
 <label>Kode Matkul</label>
 <input type="text" name="kd_matkul" value="<?php echo $data['kd_matkul']; ?>" autofocus="" required="" />
 </div>
 <div>
 <label>Nama Matkul</label>
 <input type="text" name="nama_matkul" value="<?php echo $data['nama_matkul']; ?>"/>
 </div>
 <div>
 <label>SKS</label>
 <input type="text" name="sks" value="<?php echo $data['sks']; ?>" />
 </div>
 <div>
 <label>Jadwal</label>
 <input type="text" name="jadwal"  value="<?php echo $data['jadwal']; ?>"/>
 </div>
 <div>
 <label>Jam Mulai</label>
 <input type="text" name="mulai"  value="<?php echo $data['mulai']; ?>"/>
 </div>
 <div>
 <label>Jam Berakhir</label>
 <input type="text" name="berakhir"  value="<?php echo $data['berakhir']; ?>"/>
 </div>
 <div>
 <label>Dosen Pengampu</label>
 <input type="text" name="dosen"  value="<?php echo $data['dosen']; ?>"/>
 </div>
 <div>
 <button type="submit">Simpan Perubahan</button>
 </div>
 </section>
 </form
 </html>